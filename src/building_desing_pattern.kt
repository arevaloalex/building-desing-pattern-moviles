/**
 * La interfaz del Builder define todas las formas posibles de configurar un producto.
 */
interface Builder {
    fun setType(type: Type)
    fun setSeats(seats: Int)
    fun setEngine(engine: Engine)
    fun setTransmission(transmission: Transmission)
    fun setTripComputer(tripComputer: TripComputer)
    fun setGPSNavigator(gpsNavigator: GPSNavigator)
}

/**
 * Los Builders implementan los pasos definidos en la interfaz.
 */
class CarBuilder : Builder {
    private lateinit var type: Type
    private var seats: Int = 0
    private lateinit var engine: Engine
    private lateinit var transmission: Transmission
    private lateinit var tripComputer: TripComputer
    private lateinit var gpsNavigator: GPSNavigator

    val result: Car
        get() = Car(type, seats, engine, transmission, tripComputer, gpsNavigator)

    override fun setType(type: Type) {
        this.type = type
    }

    override fun setSeats(seats: Int) {
        this.seats = seats
    }

    override fun setEngine(engine: Engine) {
        this.engine = engine
    }

    override fun setTransmission(transmission: Transmission) {
        this.transmission = transmission
    }

    override fun setTripComputer(tripComputer: TripComputer) {
        this.tripComputer = tripComputer
    }

    override fun setGPSNavigator(gpsNavigator: GPSNavigator) {
        this.gpsNavigator = gpsNavigator
    }
}

/**
A diferencia de otros patrones de creación,
Builder puede construir productos no relacionados,
que no tienen la interfaz común. En este caso, creamos un manual de
usuario para un automóvil, utilizando los mismos pasos que construimos
un automóvil. Esto permite producir manuales para modelos de
automóviles específicos, configurados con diferentes características.
 */
class CarManualBuilder : Builder {
    private lateinit var type: Type
    private var seats: Int = 0
    private lateinit var engine: Engine
    private lateinit var transmission: Transmission
    private lateinit var tripComputer: TripComputer
    private lateinit var gpsNavigator: GPSNavigator

    val result: Manual
        get() = Manual(type, seats, engine, transmission, tripComputer, gpsNavigator)

    override fun setType(type: Type) {
        this.type = type
    }

    override fun setSeats(seats: Int) {
        this.seats = seats
    }

    override fun setEngine(engine: Engine) {
        this.engine = engine
    }

    override fun setTransmission(transmission: Transmission) {
        this.transmission = transmission
    }

    override fun setTripComputer(tripComputer: TripComputer) {
        this.tripComputer = tripComputer
    }

    override fun setGPSNavigator(gpsNavigator: GPSNavigator) {
        this.gpsNavigator = gpsNavigator
    }
}

/**
 * Car es una clase producto.
 */
class Car(val type: Type, val seats: Int, val engine: Engine, val transmission: Transmission, val tripComputer: TripComputer, val gpsNavigator: GPSNavigator) {
    var fuel = 0.0

    init {
        this.tripComputer.setCar(this)
    }
}

/**
 * Car manual es otro producto
 */
class Manual(
    private val type: Type, private val seats: Int, private val engine: Engine, private val transmission: Transmission,
    private val tripComputer: TripComputer?, private val gpsNavigator: GPSNavigator?
) {

    fun print(): String {
        var info = ""
        info += "Type of car: $type\n"
        info += "Count of seats: $seats\n"
        info += "Engine: volume - " + engine.volume + "; mileage - " + engine.mileage + "\n"
        info += "Transmission: $transmission\n"
        if (this.tripComputer != null) {
            info += "Trip Computer: Functional" + "\n"
        } else {
            info += "Trip Computer: N/A" + "\n"
        }
        if (this.gpsNavigator != null) {
            info += "GPS Navigator: Functional" + "\n"
        } else {
            info += "GPS Navigator: N/A" + "\n"
        }
        return info
    }
}

enum class Type {
    CITY_CAR, SPORTS_CAR, SUV
}

/**
 * Otra característica de un automóvil.
 */
class Engine(val volume: Double, mileage: Double) {
    var mileage: Double = 0.toDouble()
        private set
    var isStarted: Boolean = false
        private set

    init {
        this.mileage = mileage
    }

    fun on() {
        isStarted = true
    }

    fun off() {
        isStarted = false
    }

    fun go(mileage: Double) {
        if (isStarted) {
            this.mileage += mileage
        } else {
            print("Cannot go(), you must start engine first!")
        }
    }
}

/**
 * Otra característica de un automóvil.
 */
class GPSNavigator {
    var route: String? = null
        private set

    constructor() {
        this.route = "221b, Baker Street, London  to Scotland Yard, 8-10 Broadway, London"
    }

    constructor(manualRoute: String) {
        this.route = manualRoute
    }
}

/**
 * Otra característica de un automóvil.
 */
enum class Transmission {
    SINGLE_SPEED, MANUAL, AUTOMATIC, SEMI_AUTOMATIC
}

/**
 * Otra característica de un automóvil.
 */
class TripComputer {

    private var car: Car? = null

    fun setCar(car: Car) {
        this.car = car
    }

    fun showFuelLevel() {
        print("Fuel level: " + car!!.fuel)
    }

    fun showStatus() {
        if (this.car!!.engine.isStarted) {
            print("Car is started")
        } else {
            print("Car isn't started")
        }
    }
}

/**
El director define el orden de los pasos de construcción.
Funciona con un objeto de generador a través de la interfaz de generador común.
Por lo tanto, puede no saber qué producto se está construyendo.
 */
class Director {

    fun constructSportsCar(builder: Builder) {
        builder.setType(Type.SPORTS_CAR)
        builder.setSeats(2)
        builder.setEngine(Engine(3.0, 0.0))
        builder.setTransmission(Transmission.SEMI_AUTOMATIC)
        builder.setTripComputer(TripComputer())
        builder.setGPSNavigator(GPSNavigator())
    }

    fun constructCityCar(builder: Builder) {
        builder.setType(Type.CITY_CAR)
        builder.setSeats(2)
        builder.setEngine(Engine(1.2, 0.0))
        builder.setTransmission(Transmission.AUTOMATIC)
        builder.setTripComputer(TripComputer())
        builder.setGPSNavigator(GPSNavigator())
    }

    fun constructSUV(builder: Builder) {
        builder.setType(Type.SUV)
        builder.setSeats(4)
        builder.setEngine(Engine(2.5, 0.0))
        builder.setTransmission(Transmission.MANUAL)
        builder.setGPSNavigator(GPSNavigator())
    }
}


fun main(args: Array<String>) {
    val director = Director()

    // Director obtiene el objeto constructor del cliente
    // Esto se debe a que la aplicación sabe mejor qué generador usar para obtener un producto específico.
    val builder = CarBuilder()
    director.constructCityCar(builder)

    // El producto final a menudo se recupera de un objeto generador, ya que
    // El director no es consciente y no depende de constructores y productos.
    val car = builder.result
    print("Car built:\n" + car.type)


    val manualBuilder = CarManualBuilder()

    // El director puede conocer varias recetas de construcción.
    director.constructCityCar(manualBuilder)
    val carManual = manualBuilder.result
    print("\nCar manual built:\n" + carManual.print())
}


